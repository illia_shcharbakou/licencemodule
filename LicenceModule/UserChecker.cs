﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace LicenceModule
{
    public class UserChecker
    {
        internal static IntPtr WTS_CURRENT_SERVER_HANDLE = IntPtr.Zero;
        internal static int WTS_UserName = 5;
        public bool SameUser;

        [DllImport("Wtsapi32.dll")]
        public static extern bool WTSQuerySessionInformationW(
            IntPtr hServer,
            int SessionId,
            int WTSInfoClass,
            out IntPtr ppBuffer,
        out IntPtr pBytesReturned);

        public bool IsSameUser(IEnumerable<int> sessionIds)
        {
            foreach (var sessionId in sessionIds)
            {
                IntPtr answerBytes;
                IntPtr answerCount;
                if (WTSQuerySessionInformationW(WTS_CURRENT_SERVER_HANDLE, sessionId, WTS_UserName, out answerBytes, out answerCount))
                {
                    if (Marshal.PtrToStringUni(answerBytes) == Environment.UserName)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
