﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenceModule
{
    /// <summary>Interface which may be used to reference a <see cref="ReadOnlyLicense"/> or <see cref="SelfSignedLicense"/> object.</summary>
    public interface ISimpleTextEditorLicense
    {
        bool IsTrial { get; }
        DateTime EffectiveStartDate { get; }
        DateTime EffectiveEndDate { get; }
        bool Validate();
        bool LoadFile();
    }
}
