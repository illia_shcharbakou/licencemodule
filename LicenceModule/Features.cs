﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenceModule
{
    public class Features
    {
        #region Private Member Variables
        private Dictionary<string, Feature> m_Features = new Dictionary<string, Feature>();
        #endregion

        #region Public Constructors
        /// <summary>Default constructor</summary>
        public Features() { }
        #endregion

        #region Public Properties
        /// <summary>List of features</summary>
        public Dictionary<string, Feature> ListFeatures
        {
            get { return m_Features; }
        }
        #endregion

        #region Public Methods
        /// <summary>Check the status of the feature whether enabled or disabled</summary>
        /// <param name="featureName">string - name of the feature</param>
        /// <returns>bool</returns>
        public bool CheckStatus(string featureName)
        {
            if (m_Features.ContainsKey(featureName))
            {
                return m_Features[featureName].Enabled;
            }
            return false;
        }

        //public string GetSchema(string featureName)
        //{
        //    return ProductConfiguration.GetSchemaName(m_Features[featureName].ProductType);
        //}

        /// <summary>Adds feature to the list object</summary>
        /// <param name="feature">Feature-object of Feature</param>
        public void AddFeature(Feature feature)
        {
            m_Features.Add(feature.Name, feature);
        }
        #endregion
    }
}
