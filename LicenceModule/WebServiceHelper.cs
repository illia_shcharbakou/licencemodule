﻿using System.Configuration;
using System.Net;
using System.Web.Services.Protocols;
using System.Windows.Forms;
using com.softwarekey.Client.Licensing;
using com.softwarekey.Client.Utils;
using com.softwarekey.Client.WebService.XmlActivationService;
using com.softwarekey.Client.WebService.XmlLicenseFileService;
using com.softwarekey.Client.WebService.XmlLicenseService;
using com.softwarekey.Client.WebService.XmlNetworkFloatingService;

namespace LicenceModule
{
    /// <summary>Web service helper class, which helps with setting endpoint URLs, proxy server settings, etc...</summary>
    internal class WebServiceHelper
    {
        #region Private Member Variables
        private InternetConnectionInformation m_ConnectionInformation = null;
        private NetworkCredential m_ProxyAuthenticationCredentials = null;
        private LicenseError m_LastError = new LicenseError(LicenseError.ERROR_NONE);

        // TODO: If you are using your own domain name (instead of secure.softwarekey.com) for SOLO Server, you should update the default values for these URLs.
        private string m_XmlActivationServiceUrl = "https://secure.softwarekey.com/solo/webservices/XmlActivationService.asmx";
        private string m_XmlLicenseFileServiceUrl = "https://secure.softwarekey.com/solo/webservices/XmlLicenseFileService.asmx";
        private string m_XmlLicenseServiceUrl = "https://secure.softwarekey.com/solo/webservices/XmlLicenseService.asmx";
        private string m_XmlNetworkFloatingServiceUrl = "https://secure.softwarekey.com/solo/webservices/XmlNetworkFloatingService.asmx";
        #endregion

        #region Constructors
        /// <summary>Creates a new <see cref="WebServiceHelper"/> object.</summary>
        public WebServiceHelper()
        {
            this.Initialize();
        }
        #endregion

        #region Private Methods
        /// <summary>Initializes the connection information object</summary>
        private void Initialize()
        {
            //start by initializing any URL overrides in the app.config file.
            //TODO: This is here as a convenience in the sample applications, and it should be removed!  These values should never be initialized from the app.config file in your application!
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["XmlActivationServiceUrl"]))
            {
                this.m_XmlActivationServiceUrl = ConfigurationManager.AppSettings["XmlActivationServiceUrl"];
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["XmlLicenseFileServiceUrl"]))
            {
                this.m_XmlLicenseFileServiceUrl = ConfigurationManager.AppSettings["XmlLicenseFileServiceUrl"];
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["XmlLicenseServiceUrl"]))
            {
                this.m_XmlLicenseServiceUrl = ConfigurationManager.AppSettings["XmlLicenseServiceUrl"];
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["XmlNetworkFloatingServiceUrl"]))
            {
                this.m_XmlNetworkFloatingServiceUrl = ConfigurationManager.AppSettings["XmlNetworkFloatingServiceUrl"];
            }

            //now that the URLs are initialized, we can proceed to initialize the connection information:
            //TODO: if you wish to test the connection with your own web site/server, change the timeout
            //      for the test (which is 10 seconds by default), and/or specify a proxy server to use
            //      for the test (none or the system default is used by default), then you should call
            //      the appropriate overload below.
            this.m_ConnectionInformation = new InternetConnectionInformation(this.m_XmlActivationServiceUrl);
        }

        /// <summary>Initializes proxy authentication credentials only when they are not already initialized.</summary>
        /// <returns>Returns true if we have proxy authentication credentials to use.</returns>
        private bool InitializeProxyAuthenticationCredentials()
        {
            if (null != this.m_ProxyAuthenticationCredentials)
            {
                return true;
            }

            return this.ShowProxyAuthenticationCredentialsDialog();
        }

        /// <summary>Shows the proxy authentication credentials dialog to obtain the credentials from the user.</summary>
        /// <returns>Returns true if we got data from the proxy authentication credentials dialog.</returns>
        private bool ShowProxyAuthenticationCredentialsDialog()
        {
            //we don't have any credentials, so let's ask for them...
            using (ProxyCredentialsForm credentialForm = new ProxyCredentialsForm())
            {
                if (DialogResult.OK == credentialForm.ShowDialog())
                {
                    this.m_ProxyAuthenticationCredentials = credentialForm.Credentials;
                    return true;
                }
            }

            return false;
        }

        /// <summary>Initializes a web service object.</summary>
        /// <param name="ws">The object which will be used to call web service methods.</param>
        /// <returns>Returns true if the object was initialized and is considered usable.</returns>
        private bool InitializeWebServiceObject(ref SoapHttpClientProtocol ws)
        {
            if (this.m_ConnectionInformation.ProxyRequired)
            {
                if (this.m_ConnectionInformation.ProxyAuthenticationRequired)
                {
                    //this connection requires proxy authentication...
                    if (null == this.m_ProxyAuthenticationCredentials)
                    {
                        //no proxy authentication credentials have been entered yet, so let's ask for them...
                        bool done = false;
                        bool dialogResult = true;
                        do
                        {
                            //clear any credentials entered earlier and ask the user for new credentials
                            this.ResetProxyAuthenticationCredentials();
                            dialogResult = this.ShowProxyAuthenticationCredentialsDialog();
                            if (!dialogResult)
                            {
                                //the user clicked cancel on the dialog
                                return false;
                            }

                            //cache this info in our proxy object
                            this.m_ConnectionInformation.Proxy.Credentials = this.m_ProxyAuthenticationCredentials;

                            //test the proxy authentication info -- if it fails, we should ask the user for credentials again
                            done = this.m_ConnectionInformation.RunTestRequest();
                        } while (!done);
                    }
                }

                //now set the web service object's proxy information
                ws.Proxy = this.m_ConnectionInformation.Proxy;
            }

            return true;
        }
        #endregion

        #region Public Read-Only Properties
        /// <summary>The <see cref="InternetConnectionInformation"/> object, which contains data about the application's Internet connectivity.</summary>
        public InternetConnectionInformation ConnectionInformation
        {
            get { return this.m_ConnectionInformation; }
        }

        /// <summary>The <see cref="LicenseError"/> object, containing details about the last error that occurred.</summary>
        public LicenseError LastError
        {
            get { return this.m_LastError; }
        }
        #endregion

        #region Public Properties
        /// <summary>Contains the username and password credentials used for authenticating with a proxy server (may be null if unused).</summary>
        public NetworkCredential ProxyAuthenticationCredentials
        {
            get { return this.m_ProxyAuthenticationCredentials; }
            set { this.m_ProxyAuthenticationCredentials = value; }
        }
        #endregion

        #region Public Methods
        /// <summary>Resets/disregards any prior proxy authentication credentials that may have been entered.</summary>
        public void ResetProxyAuthenticationCredentials()
        {
            this.m_ProxyAuthenticationCredentials = null;
        }

        /// <summary>Gets a new <see cref="XmlActivationService"/> object, which may be used for processing web service methods centered around activation.</summary>
        /// <returns>Returns a new <see cref="XmlActivationService"/> object.</returns>
        public XmlActivationService GetNewXmlActivationServiceObject()
        {
            //create the new web service object with our URL
            SoapHttpClientProtocol ws = new XmlActivationService();
            ws.Url = this.m_XmlActivationServiceUrl;

            if (!this.InitializeWebServiceObject(ref ws))
                return null;

            //return the web service object
            return (XmlActivationService)ws;
        }

        /// <summary>Gets a new <see cref="XmlLicenseFileService"/> object, which may be used for processing web service methods centered around XML license file calls to SOLO Server.</summary>
        /// <returns>Returns a new <see cref="XmlLicenseFileService"/> object.</returns>
        public XmlLicenseFileService GetNewXmlLicenseFileServiceObject()
        {
            //create the new web service object with our URL
            SoapHttpClientProtocol ws = new XmlLicenseFileService();
            ws.Url = this.m_XmlLicenseFileServiceUrl;

            if (!this.InitializeWebServiceObject(ref ws))
                return null;

            //return the web service object
            return (XmlLicenseFileService)ws;
        }

        /// <summary>Gets a new <see cref="XmlLicenseService"/> object, which may be used for processing web service methods centered around XML license file calls to SOLO Server.</summary>
        /// <returns>Returns a new <see cref="XmlLicenseService"/> object.</returns>
        public XmlLicenseService GetNewXmlLicenseServiceObject()
        {
            //create the new web service object with our URL
            SoapHttpClientProtocol ws = new XmlLicenseService();
            ws.Url = this.m_XmlLicenseServiceUrl;

            if (!this.InitializeWebServiceObject(ref ws))
                return null;

            //return the web service object
            return (XmlLicenseService)ws;
        }

        /// <summary>Gets a new <see cref="XmlNetworkFloatingService"/> object, which may be used for processing web service methods centered around network floating licensing via SOLO Server.</summary>
        /// <returns>Returns a new <see cref="XmlNetworkFloatingService"/> object.</returns>
        public XmlNetworkFloatingService GetNewXmlNetworkFloatingServiceObject()
        {
            //create the new web service object with our URL
            SoapHttpClientProtocol ws = new XmlNetworkFloatingService();
            ws.Url = this.m_XmlNetworkFloatingServiceUrl;

            if (!this.InitializeWebServiceObject(ref ws))
                return null;

            //return the web service object
            return (XmlNetworkFloatingService)ws;
        }
        #endregion
    }
}
