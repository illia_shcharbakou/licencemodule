﻿using System;
using System.Windows;

namespace LicenceModule
{
    /// <summary>
    /// Interaction logic for ActivateManuallyWindow.xaml
    /// </summary>
    public partial class ActivateManuallyWindow : Window
    {
        public ActivateManuallyWindow()
        {
            InitializeComponent();
        }
    }
}
