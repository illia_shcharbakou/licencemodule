﻿using System;

namespace LicenceModule
{
    /// <summary>SplashScreen class implementation</summary>
    public static class SplashScreen
    {
        #region Private Static Member Variables
        //private static SplashScreenForm m_SplashForm = null;
        #endregion

        #region Public Static Methods
        /// <summary>Displays the SplashScreen</summary>
        public static void ShowSplashScreen()
        {
            //if (m_SplashForm == null)
            //{
            //    m_SplashForm = new SplashScreenForm();
            //    m_SplashForm.ShowSplashScreen();
            //}
        }

        /// <summary>Closes the SplashScreen</summary>
        public static void CloseSplashScreen()
        {
            //if (m_SplashForm != null)
            //{
            //    m_SplashForm.CloseSplashScreen();
            //    m_SplashForm = null;
            //}
        }

        /// <summary>Update text in default of success message</summary>
        /// <param name="Text">string - Message</param>
        public static void UdpateStatusText(string Text)
        {
            //if (m_SplashForm != null)
            //    m_SplashForm.UdpateStatusText(Text);
        }

        /// <summary>Initializes application features</summary>
        /// <param name="features">Features - object</param>
        /// <param name="license">ReadOnlyLicense - license object</param>
        /// <param name="successful">Whether license is reloaded successfully or not</param>
        public static void InitializeFeatures(Features features, ReadOnlyLicense license, bool successful)
        {
            license.GetLicenseFromFile();
            var listOfAvailableLicenses = license.LicenseTypes;
            foreach (var feature in listOfAvailableLicenses)
            {
                if (features.ListFeatures.Count > 0)
                {
                    features.ListFeatures.Remove(feature.DisplayName);
                }
                features.AddFeature(new Feature(feature.DisplayName, !successful ? false : license.GetFeatureEnabled(feature.Id), feature.DisplayName, feature.ProductType));
            }
        }

        /// <summary>If license is trial license enabled all application features</summary>
        /// <param name="features">Features - object</param>
        /// <param name="license">ReadOnlyLicense - license object</param>
        //public static void InitializeTrialFeatures(Features features, ReadOnlyLicense license)
        //{
            //Enum e = ReadOnlyLicense.LicenseFeatures.Importer;
            //foreach (object name in Enum.GetValues(e.GetType()))
            //{
            //    features.AddFeature(new Feature(name.ToString(), true, license.GetDisplayName((ReadOnlyLicense.LicenseFeatures)name)));
            //}
        //}
        #endregion
    }
}