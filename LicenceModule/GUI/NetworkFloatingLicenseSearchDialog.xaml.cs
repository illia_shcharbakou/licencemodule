﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using com.softwarekey.Client.Licensing.Network;

namespace LicenceModule
{
    /// <summary>
    /// Interaction logic for NetworkFloatingLicenseSearchDialog.xaml
    /// </summary>
    public partial class NetworkFloatingLicenseSearchDialog : Window
    {
        #region Private Member Variables
        private NetworkSemaphore m_semaphore;
        #endregion

        public NetworkFloatingLicenseSearchDialog(NetworkSemaphore semaphore)
        {
            m_semaphore = semaphore;

            InitializeComponent();
        }

        //#region Private Methods
        //private void Window_Loaded(object sender, RoutedEventArgs e)
        //{
        //    m_semaphore.SearchProgress += new NetworkSemaphore.SearchProgressEventHandler(SearchProgress);
        //    m_semaphore.SearchCompleted += new NetworkSemaphore.SearchCompletedEventHandler(SearchCompleted);

        //    m_semaphore.Search();
        //}
        //private delegate void SearchThreadCompletedDelegate(object sender, SearchCompletedEventArgs e);

        ///// <summary>Search completed</summary>
        ///// <param name="sender">object</param>
        ///// <param name="e">EventArgs</param>
        //void SearchCompleted(object sender, SearchCompletedEventArgs e)
        //{
        //    if (this.InvokeRequired)
        //    {
        //        this.Invoke(new SearchThreadCompletedDelegate(SearchCompleted), new Object[] { sender, e }); // invoke this method using our UI thread delegate
        //    }
        //    else
        //    {
        //        if (e.SeatOpened)
        //        {
        //            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        //            this.Close();
        //        }
        //        else
        //        {
        //            this.DialogResult = DialogResult.Cancel;
        //            this.Close();
        //        }
        //    }
        //}

        //private delegate void SearchThreadProgressDelegate(object sender, SearchProgressEventArgs e);

        ///// <summary>Search progress</summary>
        ///// <param name="sender">object</param>
        ///// <param name="e">EventArgs</param>
        //private void SearchProgress(object sender, SearchProgressEventArgs e)
        //{
        //    if (this.InvokeRequired)
        //    {
        //        this.Invoke(new SearchThreadProgressDelegate(SearchProgress), new Object[] { sender, e }); // invoke this method using our UI thread delegate
        //    }
        //    else
        //    {
        //        progressBar.Visible = true;
        //        progressBar.Value = e.ProgressPercentage;
        //    }
        //}

        //private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        //{
        //    m_semaphore.CancelSearch();
        //}

        //private void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    this.Close();
        //}
        //#endregion
    }
}
