﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Media;
using ArkkUtilities;
//using AssemblyInformation;
using System.Reflection;
using Microsoft.Win32;

namespace LicenceModule
{
    /// <summary>
    /// Interaction logic for NetworkLicenseBrowseDialog.xaml
    /// </summary>
    public partial class NetworkLicenseBrowseDialog : Window
    {
        string m_licenseRegistryLocation;
        public NetworkLicenseBrowseDialog(string licenseRegistryLocation)
        {
            InitializeComponent();
            try
            {
                m_licenseRegistryLocation = licenseRegistryLocation;

                RegistryWrap registry = new RegistryWrap(typeof(AppHelper));
                textBoxPath.Text = registry.GetValue("Licence Path", string.Empty, Registry.LocalMachine);

                if (string.IsNullOrEmpty(textBoxPath.Text))
                {
                    if (!File.Exists(AppHelper.m_LicenseFilePath))
                    {
                        lblInvalidPathLabel.Text = "The licence could not be found in selected path.";
                        lblInvalidPathLabel.Visibility = Visibility.Visible;
                    }
                    textBoxPath.Text = Path.GetDirectoryName(AppHelper.m_LicenseFilePath) + "\\";
                }
                   
                        
                else
                    textBoxPath.Foreground = new SolidColorBrush(Colors.Black);
                buttonOK.Focus();
            }
            catch { }
        }

        #region Private Member Variables
        private string m_selectedPath = "";

        // TODO: update this constant to a registry location of your choosing (located under HKEY_CURRENT_USER).
        
        #endregion

        #region Public Properties
        /// <summary>The path selected by the user.</summary>
        public string SelectedPath
        {
            get { return m_selectedPath; }
            set { m_selectedPath = value; }
        }
        #endregion

        #region Private Methods
        /// <summary>Ok button click event handler</summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void buttonOK_Click(object sender, RoutedEventArgs e)
        {
            if (!Directory.Exists(textBoxPath.Text))
            {
                //errorProvider.SetError(textBoxPath, "The specified path is invalid or unavailable.");
                lblInvalidPathLabel.Visibility = Visibility.Visible;
            }
            else
            {
                if (!textBoxPath.Text.EndsWith("\\")) 
                    textBoxPath.Text = textBoxPath.Text + "\\";

                var licenseFilePath = Path.Combine(textBoxPath.Text, AppHelper.SoftwareInfo.LicenceFileName);
                
                if (!File.Exists(licenseFilePath))
                {
                    lblInvalidPathLabel.Text = "The licence could not be found in selected path.";
                    lblInvalidPathLabel.Visibility = Visibility.Visible;
                }
                else
                {
                    m_selectedPath = textBoxPath.Text;
                    var registry = new RegistryWrap(typeof(NetworkLicenseBrowseDialog));
                    var pathInRegistry = registry.GetValue("Licence Path", string.Empty, Registry.LocalMachine);
                    if (pathInRegistry != textBoxPath.Text)
                    {
                        CreateLicencePathInRegistry(textBoxPath.Text);
                    }
                    licenseFilePath = Path.GetDirectoryName(AppHelper.m_LicenseFilePath) + "\\";
                    if ((m_selectedPath == textBoxPath.Text && !AppHelper.IsNetworkPath(m_selectedPath)) ||
                        (AppHelper.IsVirtualMachine() && m_selectedPath != licenseFilePath))
                    {
                        AppHelper.ReleaseRepositoryCache();
                    }
                    this.DialogResult = true;
                    this.Close();
                }
            }
        }

        private void CreateLicencePathInRegistry(string textPath)
        {
            const string licencePath = "\"Licence Path\"";
            var licencePathArguments = licencePath + " " + textPath;
            ProcessStartInfo psi = new ProcessStartInfo();
            string currentLocation = Assembly.GetExecutingAssembly().CodeBase;
            psi.FileName = Path.Combine(Path.GetDirectoryName(currentLocation).Substring(6), "SetupRegistry.exe");
            psi.Arguments = licencePathArguments; 
            psi.WindowStyle = ProcessWindowStyle.Hidden;
            try
            {
                using (Process proc = Process.Start(psi))
                {
                    proc.WaitForExit();
                }
            }
            catch
            {
                m_selectedPath = Path.GetDirectoryName(AppHelper.m_LicenseFilePath) + "\\";
            }
        }

        /// <summary>Text box path click event handler</summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void textBoxPath_Click(object sender, EventArgs e)
        {
            //errorProvider.Clear();
            lblInvalidPathLabel.Visibility = Visibility.Hidden;

            if (textBoxPath.Text == @"Example: \\server\share")
            {
                textBoxPath.Text = "";
                textBoxPath.Foreground = new SolidColorBrush(Colors.Black);
            }
        }

        /// <summary>Simple open file folder dialog click event handler</summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void SimpleOpenFileFolderDialog_Click(object sender, EventArgs e)
        {
            if (textBoxPath.Text == "")
            {
                textBoxPath.Text = @"Example: \\server\share";
                textBoxPath.Foreground = new SolidColorBrush(Colors.DarkGray);

                if (textBoxPath.IsFocused)
                    buttonBrowse.Focus();
            }
        }

        /// <summary>Text box path enter event handler</summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void textBoxPath_Enter(object sender, EventArgs e)
        {
            if (textBoxPath.Text == @"Example: \\server\share")
            {
                textBoxPath.Text = "";
                textBoxPath.Foreground = new SolidColorBrush(Colors.DarkGray);
            }
        }
        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void buttonBrowse_Click(object sender, RoutedEventArgs e)
        {
            //errorProvider.Clear();
            lblInvalidPathLabel.Visibility = Visibility.Hidden;

            var fbDlg = new System.Windows.Forms.FolderBrowserDialog();

            var res = fbDlg.ShowDialog();
            if (res == System.Windows.Forms.DialogResult.OK)
            {
                textBoxPath.Text = fbDlg.SelectedPath;

                textBoxPath.Foreground = new SolidColorBrush(Colors.Black);
                buttonOK.Focus();
            }
        }

        private void defaultPath_Click(object sender, RoutedEventArgs e)
        {
            var licenseDefaultPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\ArkkSolutions\\";
            var licenseFilePath = Path.Combine(licenseDefaultPath, AppHelper.SoftwareInfo.LicenceFileName);
            if (!File.Exists(licenseFilePath))
            {
                lblInvalidPathLabel.Text = "The licence could not be found in selected path.";
                lblInvalidPathLabel.Visibility = Visibility.Visible;
            }
            textBoxPath.Text = licenseDefaultPath;
        }
    }
}
