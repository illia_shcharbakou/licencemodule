﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace LicenceModule
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow : Window
    {
        private ReadOnlyLicense m_license;
        private readonly string m_licenseRegistryLocation;

        private AppHelper.UpdateRibbonDelegate _ribbonDelegate; 

        /// <summary>Default constructor</summary>
        public AboutWindow(ReadOnlyLicense license, string licenseRegistryLocation, AppHelper.UpdateRibbonDelegate ribbonDelegate)
        {
            m_license = license;
            m_licenseRegistryLocation = licenseRegistryLocation;
            InitInternalCacheIfLicencePathInRegistryHasChanged(ribbonDelegate);
            _ribbonDelegate = ribbonDelegate;
            Title = AppHelper.SoftwareInfo.ProductTitle;
            
            InitializeComponent();
        }
        /// <summary>
        /// Initiate Internal Cache if the current LicencePath is different to the one in the Registry.
        /// That happens when we are using Ixbrl and the LicencePath is changed by Aifmd or Corep.
        /// </summary>
        /// <param name="ribbonDelegate"></param>
        private void InitInternalCacheIfLicencePathInRegistryHasChanged(AppHelper.UpdateRibbonDelegate ribbonDelegate)
        {
            var registryLicencePath = new NetworkLicenseBrowseDialog(m_licenseRegistryLocation).textBoxPath.Text; 
            var currentLicenseFilePath = Path.GetDirectoryName(AppHelper.m_LicenseFilePath) + "\\";
            if (registryLicencePath != currentLicenseFilePath)
            {
                var licenseFilePath = Path.Combine(registryLicencePath, AppHelper.SoftwareInfo.LicenceFileName);
                if (File.Exists(licenseFilePath))
                {
                    var app = AppHelper.ApplicationName.Substring(0, AppHelper.ApplicationName.Length - 1);
                    var successful = AppHelper.InitInternalCache(m_licenseRegistryLocation, app, ribbonDelegate, true);
                    if (successful)
                    {
                        AppHelper.m_LicenseFilePath = licenseFilePath;
                    }
                }
            }
        }

        /// <summary>Form load implementation</summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;

            lblVersion.Text = version.Major + "." + 
                              version.Minor + "." + 
                              version.Build + "." +
                              version.MinorRevision;
            LoadStatus();
        }
        /// <summary>OK button click event handler</summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void OK_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>Activate Manually button click event handler</summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void ActivateManually_Click(object sender, RoutedEventArgs e)
        {
            //com.softwarekey.Client.Samples.SimpleTextEditorSelfSignedTrial.ActivateManuallyForm activationDialog = null;
            //try
            //{
            //    //activationDialog = new com.softwarekey.Client.Samples.SimpleTextEditorSelfSignedTrial.ActivateManuallyForm(this);
            //    activationDialog.ShowDialog();
            //    AddInServices.InitializeFeaturesWithLicense();
            //}
            //finally
            //{
            //    activationDialog.Dispose();
            //}
        }

        /// <summary>Activate Online button click event handler</summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void ActivateOnline_Click(object sender, RoutedEventArgs e)
        {
            ActivateOnlineWindow activationDialog = null;
            try
            {
                activationDialog = new ActivateOnlineWindow(this, m_license);
                activationDialog.ShowDialog();
                if (AppHelper.InitializeFeaturesWithLicense())
                {
                    if (_ribbonDelegate != null)
                    {
                        _ribbonDelegate(AppHelper.AllFeatures);
                    }

                }
                LoadStatus();
            }
            finally
            {
                activationDialog.Close();
            }
        }

        /// <summary>Refresh Licence button click event handler</summary>
        /// <param name="sender">object</param>
        /// <param name="e">EventArgs</param>
        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            if (AppHelper.License.RefreshLicense())
            {
                AppHelper.InitializeFeaturesWithLicense();
                if (_ribbonDelegate != null)
                {
                    _ribbonDelegate(AppHelper.AllFeatures);
                }
                MessageBox.Show("The licence has been refreshed successfully.", "Licence Refresh", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("The licence was not refreshed.  Error: (" + AppHelper.License.LastError.ErrorNumber + ")" + AppHelper.License.LastError.ErrorString, "Licence Refresh", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            AppHelper.InitializeFeaturesWithLicense();
            //AddInServices.ReloadLicense();
            LoadStatus();
        }

        public void AppendStatus(string str)
        {
            if (lblStatus.Text.Length > 0)
                lblStatus.Text += "\n";

            lblStatus.Text += str;
        }

        public void UpdateStatus(string str)
        {
            lblStatus.Text = str;
        }

        /// <summary>Loads the status</summary>
        public void LoadStatus()
        {
            if (!String.IsNullOrEmpty(AppHelper.License.LicenseStatus))
            {
                lblStatus.Text = "";
                lblStatus.Text = AppHelper.License.LicenseStatus;
            }
            //if ((!AddInServices.IsTrial && AddInServices.IsLicenseValid) ||(AddInServices.IsTrial))
            if (AppHelper.IsLicenseValid)
                RefreshLicenseButton.Visibility = System.Windows.Visibility.Visible;
            else
                RefreshLicenseButton.Visibility = System.Windows.Visibility.Hidden;

            int y = 10;
            int row = 0;
            pnlFeatureStatus.RowDefinitions.Clear();
            pnlFeatureStatus.Children.Clear();

            foreach (Feature feature in AppHelper.AllFeatures.ListFeatures.Values)
            {
                Grid g = new Grid();
                var r1 = new RowDefinition() { Height = new GridLength(20.0) };
                pnlFeatureStatus.RowDefinitions.Add(r1);
                //var c2 = new ColumnDefinition() { Width = new GridLength(90.0) };
                //g.ColumnDefinitions.Add(c1);
                //g.ColumnDefinitions.Add(c2);

                TextBlock label1 = new TextBlock();
                label1.Name = "label1" + y;
                label1.Height = 20;
                label1.Text = feature.DisplayName;
                Grid.SetColumn(label1, 0);
                
                TextBlock label2 = new TextBlock();
                label2.Name = "label2" + y;
                label2.Height = 20;
                //label2.TabIndex = 0;
                y = y + 20;

                
                if (feature.Enabled)
                {
                    label2.Foreground = new SolidColorBrush(Colors.Green);
                    label2.Text = "Enabled";
                }
                else
                {
                    label2.Foreground = new SolidColorBrush(Colors.Red);
                    label2.Text = "Disabled";
                }
                Grid.SetRow(label1, row);
                Grid.SetColumn(label1, 0);
                Grid.SetRow(label2, row);
                Grid.SetColumn(label2, 1);
                //g.Children.Add(label1);
                //g.Children.Add(label2);
                pnlFeatureStatus.Children.Add(label1);
                pnlFeatureStatus.Children.Add(label2);

                row += 1;
            }

            if (AppHelper.License.LastError.ErrorNumber == com.softwarekey.Client.Licensing.LicenseError.ERROR_PLUS_EVALUATION_INVALID)
            {
                ActivateOnlineButton.IsEnabled = false;
                //ActivateManuallyButton.IsEnabled = false;
                RefreshLicenseButton.IsEnabled = false;
            }
        }

        private void NetworkLicense_Click(object sender, RoutedEventArgs e)
        {
            NetworkLicenseBrowseDialog browseDlg = null;
            try
            {
                browseDlg = new NetworkLicenseBrowseDialog(m_licenseRegistryLocation);
                var res = browseDlg.ShowDialog();
                if (res.Value)
                {
                    AppHelper.m_LicenseFilePath = Path.Combine(browseDlg.SelectedPath, AppHelper.SoftwareInfo.LicenceFileName);
                    AppHelper.InitializeFeaturesWithLicense();
                    LoadStatus();
                }
            }
            finally
            {
                browseDlg.Close();
            }
        }

    }
}
