﻿using System;
using System.Windows;

namespace LicenceModule
{
    /// <summary>
    /// Interaction logic for ActivateOnlineWindow.xaml
    /// </summary>
    public partial class ActivateOnlineWindow : Window
    {
        private ReadOnlyLicense m_License = null;
        private AboutWindow m_AboutForm = null;

        public ActivateOnlineWindow(AboutWindow about, ReadOnlyLicense license)
        {
            m_License = license;
            m_AboutForm = about;

            InitializeComponent();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ActivateButton_Click(object sender, RoutedEventArgs e)
        {
            Int32 licenseId = 0;
            string password = PasswordTextBox.Password;
            bool successful = false;

            if ("" == LicenseIDTextBox.Text)
            {
                MessageBox.Show("Please enter a Licence ID.", "Activation", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                LicenseIDTextBox.Focus();
                return;
            }

            if (!Int32.TryParse(LicenseIDTextBox.Text, out licenseId))
            {
                MessageBox.Show("The Licence ID may only contain numbers.", "Activation", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                LicenseIDTextBox.Focus();
                return;
            }

            if (!Int32.TryParse(LicenseIDTextBox.Text, out licenseId))
            {
                MessageBox.Show("The Licence ID may only contain numbers.", "Activation", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                LicenseIDTextBox.Focus();
                return;
            }

            if ("" == PasswordTextBox.Password)
            {
                MessageBox.Show("Please enter your password.", "Activation", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                PasswordTextBox.Focus();
                return;
            }

            if (InstallationNameTextBox.Text == String.Empty)
            {
                MessageBox.Show("Please enter a Installation Name.", "Activation", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                LicenseIDTextBox.Focus();
                return;
            }
            else
            {
                m_License.InstallationName = InstallationNameTextBox.Text;
            }

            successful = m_License.ActivateOnline(licenseId, password);

            if (successful)
            {
                MessageBox.Show("Activation Successful!", "Activation", MessageBoxButton.OK, MessageBoxImage.Information);
                //m_MainForm.InitializeFeaturesWithLicense();
                if (m_AboutForm != null)
                {
                    m_AboutForm.LoadStatus();
                }
                Close();
            }
            else
            {
                string error = "";
                switch (m_License.LastError.ExtendedErrorNumber)
                {
                    case 5000: error = "Invalid EncryptionKey ID."; break;
                    case 5001: error = "Encryption Required."; break;
                    case 5002: error = "Signature Required."; break;
                    case 5003: error = "Decryption Failure."; break;
                    case 5004: error = "Verification Failure."; break;
                    case 5005: error = "Invalid or missing parameters."; break;
                    case 5006: error = "Security Check failure: Too many failed activations."; break;
                    case 5007: error = "Invalid ComputerID."; break;
                    case 5008: error = "Invalid Licence ID and/or Password."; break;
                    case 5009: error = "Unregistered customer."; break;
                    case 5010: error = "Invalid Product."; break;
                    case 5011: error = "Invalid Product Option."; break;
                    case 5012: error = "Invalid Product Version."; break;
                    case 5013: error = "Maximum number of activations exceeded."; break;
                    case 5014: error = "Invalid Product Option type."; break;
                    case 5015: error = "Invalid Installation ID."; break;
                    case 5016: error = "This installation ID has been deactivateed."; break;
                    case 5017: error = "Invalid Licence Status please contact Arkk support at: support@arkksolutions.com"; break;
                    case 5018: error = "No remaining deactivations available for this License."; break;
                    case 5019: error = "Licence has expired."; break;
                    case 5020: error = "Internal Authentication Failed."; break;
                    case 5021: error = "Invalid IP adress."; break;
                    case 5022: error = "Invalid System time."; break;
                    case 5023: error = "Acess not enabled."; break;
                    case 5024: error = "Invalid XML Document."; break;
                    case 5025: error = "Plug-in Failed."; break;
                    case 5026: error = "Plug-in Processing Failed."; break;
                    case 5027: error = "The Network Session has been closed or has expired."; break;
                    case 5028: error = "No Network Seats Remainig."; break;
                    case 5029: error = "Invalid Trigger Code."; break;
                    case 5030: error = "API evaluation has Expired."; break;
                    case 5031: error = "Server evaluation has Expired."; break;
                }
                error = String.IsNullOrEmpty(error) ? m_License.LastError.ErrorString : error;
                MessageBox.Show("Activation Failed." + Environment.NewLine + Environment.NewLine + "Error " + m_License.LastError.ExtendedErrorNumber.ToString() + ": " + error, "Activation", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (m_License.LicenseID > 0)
            {
                LicenseIDTextBox.Text = m_License.LicenseID.ToString();
                PasswordTextBox.Focus();
            }
            else
            {
                LicenseIDTextBox.Focus();
            }
        }

    }
}
