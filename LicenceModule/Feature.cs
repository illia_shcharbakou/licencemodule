﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LicenceModule
{
    public class Feature
    {
        #region Private Member Variables
        private string m_Name = "";
        private string m_DisplayName = "";
        private bool m_Enabled = false;
        private ProductType m_ProductType = ProductType.Unknown;
        #endregion

        #region Public Constructor
        /// <summary>Creation contructor</summary>
        /// <param name="name">string - Name of the feature</param>
        /// <param name="enabled">bool - whether feature is enabled or not</param>
        public Feature(string name, bool enabled)
        {
            m_Name = name;
            m_Enabled = enabled;
            m_DisplayName = name;
        }

        /// <summary>Creation contructor with display name</summary>
        /// <param name="name">string - Name of the feature</param>
        /// <param name="enabled">bool - whether feature is enabled or not</param>
        /// <param name="displayName">string - display name</param>
        /// <param name="productType">ProductType - type of product</param>
        public Feature(string name, bool enabled, string displayName, ProductType productType)
        {
            m_Name = name;
            m_DisplayName = displayName;
            m_Enabled = enabled;
            m_ProductType = productType;
        }
        #endregion

        #region Public Properties
        /// <summary>Name</summary>
        public string Name
        {
            get { return m_Name; }
        }

        /// <summary>Display name</summary>
        public string DisplayName
        {
            get { return m_DisplayName; }
            set { m_DisplayName = value; }
        }

        /// <summary>Enabled</summary>
        public bool Enabled
        {
            get { return m_Enabled; }
            set { m_Enabled = value; }
        }

        /// <summary>ProductType</summary>
        public ProductType ProductType
        {
            get { return m_ProductType; }
            set { m_ProductType = value; }
        }
        #endregion
    }
}
