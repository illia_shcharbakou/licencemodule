﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Xml;
using LicenceModule.Properties;

namespace LicenceModule
{
    public class XmlHandler
    {
        public static List<LicenseType> ReadLicenseFeatures()
        {
            var list = new List<LicenseType>();
            try
            {
                using (var reader = XmlReader.Create(new StringReader(Resources.XmlEngine)))
                {
                    while (reader.Read())
                    {
                        if (!(reader.Name == "Feature" && reader.NodeType == XmlNodeType.Element))
                        {
                            continue;
                        }

                        KeyValuePair<Int32, string> description = new KeyValuePair<int, string>(-1, string.Empty);
                        ProductType productType = ProductType.Unknown;

                        while (!(reader.Name == "Feature" && reader.NodeType == XmlNodeType.EndElement))
                        {
                            switch (reader.Name)
                            {
                                case "Description":
                                    {
                                        if (reader.NodeType == XmlNodeType.Element)
                                        {
                                            string name = reader["name"];
                                            if (reader.Read())
                                            {
                                                int value = Convert.ToInt32(reader.Value.Trim(), 16);
                                                description = new KeyValuePair<int, string>(value, name);
                                            }
                                        }
                                        break;
                                    }
                                case "ProductType":
                                    {
                                        if (reader.NodeType == XmlNodeType.Element && reader.Read())
                                        {
                                            if (!Enum.TryParse(reader.Value.Trim(), out productType))
                                            {
                                                productType = ProductType.Unknown;
                                            }
                                        }
                                        break;
                                    }
                            }
                            reader.Read();
                        }

                        if (description.Key == -1)
                        {
                            continue;
                        }

                        var licType = new LicenseType
                        {
                            DisplayName = description.Value,
                            Id = description.Key,
                            ProductType = productType
                        };
                        list.Add(licType);

                        //if (reader.Name != "Feature" || reader.NodeType == XmlNodeType.EndElement) continue;
                        //var elemValues = GetDescription(reader);
                        //var schema = GetSchema(reader);
                        //var templateName = GetTemplateName(reader);
                        //var licType = new LicenseType
                        //{
                        //    DisplayName = elemValues.Value,
                        //    Id = elemValues.Key,
                        //    Schema = schema,
                        //    TemplateName = templateName
                        //};
                        //list.Add(licType);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error", MessageBoxButton.OK);
                throw e;
            }
            return list;
        }

        //private static string GetSchema(XmlReader reader)
        //{
        //    while (reader.Read())
        //    {
        //        if (reader.Name == "Feature" && reader.NodeType == XmlNodeType.EndElement) return string.Empty;
        //        if (!(reader.Name == "Schema" && reader.NodeType != XmlNodeType.EndElement)) continue;
        //        if (!reader.Read()) continue;
        //        return reader.Value.Trim();
        //    }
        //    return String.Empty;
        //}

        //private static KeyValuePair<Int32, string> GetDescription(XmlReader reader)
        //{
        //    while (reader.Read())
        //    {
        //        if (!(reader.Name == "Description" && reader.NodeType != XmlNodeType.EndElement)) continue;
        //        var desc = reader["name"];
        //        if (!reader.Read()) continue;
        //        var value = reader.Value.Trim();
        //        var parsedValue = Convert.ToInt32(value, 16);
        //        return new KeyValuePair<int, string>(parsedValue, desc);
        //    }
        //    return new KeyValuePair<int, string>(0, String.Empty);
        //}
    }
}
