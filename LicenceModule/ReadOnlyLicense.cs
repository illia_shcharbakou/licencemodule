﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using ArkkUtilities;
using Microsoft.Win32;
using com.softwarekey.Client.Licensing;
using com.softwarekey.Client.Utils;
using com.softwarekey.Client.WebService.XmlActivationService;
using com.softwarekey.Client.WebService.XmlLicenseFileService;

namespace LicenceModule
{
    public class LicenseType
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public ProductType ProductType { get; set; }
    }

    public class ReadOnlyLicense : com.softwarekey.Client.Licensing.License, ISimpleTextEditorLicense
    {
        public List<LicenseType> LicenseTypes { get; set; }

        public void GetLicenseFromFile()
        {
            if (LicenseTypes == null)
                LicenseTypes = new List<LicenseType>();
            var list = XmlHandler.ReadLicenseFeatures();
            foreach (var license in list)
            {
                LicenseTypes.Add(new LicenseType { DisplayName = license.DisplayName, Id = license.Id, ProductType = license.ProductType });
            }
        }

        /* #region Public Enumerations
         /// <summary>LicenseFeatures Enumeration</summary>
         public enum LicenseFeatures
         {
             [Description("Arkk Adapter")]
             Adapter = 0x01,
             [Description("Taxonomy: UK GAAP")]
             UK_GAAP = 0x02,
             [Description("Taxonomy: UK CHAR")]
             UK_CHAR = 0x04,
             [Description("Taxonomy: UK IFRS")]
             UK_IFRS = 0x08,

             [Description("Taxonomy: UK COMP")]
             UK_COMP = 0x10,
             [Description("Taxonomy: Irish GAAP")]
             IE_GAAP = 0x20,
             [Description("Taxonomy: Irish IFRS")]
             IE_IFRS = 0x40,
             [Description("Taxonomy: India GAAP")]
             IN_GAAP = 0x80,

             [Description("Taxonomy: CoRep")]
             COREP = 0x100,
             [Description("Taxonomy: FinRep")]
             FINREP = 0x200,
             [Description("Taxonomy: Carbon Disclosure")]
             CDP = 0x400,
             [Description("Arkk Tag Importer")]
             Importer = 0x800,

             [Description("Custom Taxonomies")]
             AddTaxonomy = 0x1000,
             [Description("Companies House Filing")]
             CHFiling = 0x2000,
             [Description("HMRC Validation")]
             HMRCValidation = 0x4000,
             [Description("Companies House Validation")]
             CHValidation = 0x8000,

             [Description("ROS Validation")]
             ROSValidation = 0x10000,
             [Description("Taxonomy: Singaporean")]
             Singaporean = 0x20000,
             [Description("AIFMD Adapter")]
             AIFMD = 0x40000,
             [Description("Taxonomy: UK FRS 101")]
             UK_FRS101 = 0x80000,

             [Description("Taxonomy: UK FRS 102")]
             UK_FRS102 = 0x100000,
             [Description("Taxonomy: EU IFRS")]
             EU_IFRS = 0x200000,

             [Description("Taxonomy: Asset Encumbrance")]
             ASSET_ENCUMBRANCE = 0x400000,
             [Description("Taxonomy: Funding Plans")]
             FUNDING_PLANS = 0x800000
         }
         #endregion*/

        #region Constants
        //TODO: Update the Product ID, Encryption Key ID, Client Key, and Server Key below before using in your own products!
        private const Int32 m_myProductId = 242824;
        private static readonly AuthorEncryptionKey m_EncryptionKey = new AuthorEncryptionKey("tSh7RJ4HJW/gu4N4MSq1YuvIr+ky/GhuZnDqxi1cKiODoiwinrZZSgmaoyEokWTv",
            "x1JPRPaokfgUMV6EBwPQwGzMspbkXqvXPp3C+QcAD92BU/1N4fbfqGqpag8jsCrGPi2ekMDZL46Mu4EyNpLp4dAvDm4JklhUu5hWOB/6j56lbCx9NgIqyNp8IZTm1mGRPpmY04VOlsGhftvlbk56x7wj4sEdo+kMf48JsEzGxa0UOsRHyH7jYNd52qWDFzsW1m4qNM31uov/YiBRzS80RliyTa/Gi34I+bclDqOTGKzcVY8LNhIpPb4OwLtH5aWkQm1RlsWFceGmXIqo5ogwtGevO7NyMz7CbkWkGsgg2TlOaSZk6Qo3Zk8/uUUpK/s5xZACsqyRxI908j9Ff+3HMvkTaWZtZzcjaIYy6yuvuj9ePHKbIR2ix0oVKc0KZ9glHFFyKY9yyWoR2gcc7ug5kWWQn3bzV0Vj5/IbeVGw9cCQbNss9+a/VAApigFx1DC2tTVs7XK5vz1OFXMFM6MCy8vCqupZ6Hwqpl97CRm2JLpJPfRVkMCbkICBGk88/ssh3iMDUTrUbmcHtJ9wfWAAdlhvwloQCT1n65UMGMRwCwdk0S33vVr5EfJnZgzt4mheAXZa4qGo6PI9L4Zj4oun4kvUe5Ny6t2gzpzjje1bVpWJbQXyoW5yKMZYUy5gWdKv1gSf621ZmYs9pO/L9MGtSCveimUPiI8PC4xjmm1yb0cUEPzq2uWBDD0/YjaWpbuvaSf8FB4vMUYBuaxAVyf+p440fA3hJX2S8wLYGEFS/MSouwgSi7QfyKQzbdwU7WGGgBrJD7ZkF1WiIGF+Q+lxos8tZGwH1ss7RbKEyNfyeL2bIy6XmLeTRSImV0DaNvmd2ohLni3JTnBPwQxoA5MB2KuZsjFxOxyVq9t/YwbIuvyYBqseH7p4bKt5fqpPYTtgIJipsJWmGo+KGGobIqe4kILNMLg67MjFz62YLATKOqbd24YZuSrq1GXqPletgWlbY5Axg6Qaa+OQQzNIz+6sIRbXEcH9+lX/dawpFLqAPEFfm/VuifslfCe78Us9/F2xBkY3PJlN/7oIokHRXGPS3naLqTTqRbmjwjBXQRocY2YeupA8A1VAQf6jxfYY2MjmlR7yKTqOgpEGVv8oUaHuybm4knbo2mqFyTe6Gu5q8Zw4lBYq6LaB2bydYs9HeJc8RHQl194ksejD9vY2mHXr4tFefiF0TxcN6PT9xMW+hbX/lmIBPdlJVOwSaeE5dWkS4xNCUTxjfQV4V+RXkN6yZl1ny0qtqBOB1toEmDd7q4IkEFLa3bYWon/5fX/aTocYiVFWgcb5VSCE1hZDHKXW9p2Jjr1LDBXvGqzz14zTpakW96Zr5uJVjm7hnTN+Gz32AcRAsMpiNt0Kyj81L6oBbnAt7TGmtcYhfjHfnbjMsLtyoD7eEv0MSpyNKptsLR2RIb6bqtuErSoqOgPyK83Bto9ghMuuRp/8IfJ2PZTcT66ByEOivfhbXq9sU2tQmt7cdgFtXEJIFlzUKPr0zb5IdijsrNf0W5NBfIe+N4+mLJyh9arzttsQT3w8/gpUSGz3Syc/8qOis5oLzrH/fSvT/dIXG2yLmN/b/B2n9nBcV/lHzoad9nt+hbitYjULlYW2e2H96MbKOUihMDuMu60IrsHhCoPK8NYqk1fV5t7hHbRXofHSD62Mow9Tp3hw8gvn+Mo6hj9pvZSOD90yIg1uOkQrnENuXyGbC04YGK6d76qNlJjq5vySnvC4O3Pfz9XgQCKiEzCJRvtfT2KlmUb/h33H5mw5u+n620ErChX6HzJ89JLs24Dz2DANDCoj8fCjWSSsN6OkMO2h+e2QGqsM5at+g1YcfuwFhhq66xfPUjPXho8Zx1HaUd6mDRs/LvUbXgUjckLLUwI3i5ToL/2w2FWPd1PGrJNNuUBRLfGummX+YwqwQgn/2va5c7n1GyVmJsivBb/9iFtRJCl7EyJvfInG/g7+qbMw+6HQrSwhiBR+mIaivjXQq8C7DOU4HOxGnPfdvo1azy2pTGfSLDUkzSVlV5eTrdLLfivX/08KV/YaXH6TJfvxjZivhhfsd8jksl06INe7Q2J/bbjhcAexRCoWyqamxP+fC4Y8RHVMmjbCmiUWRaOuLKbwI75WK4q5LMpd/QS7PekiUca8mxvugw==", false);
        private static readonly List<SystemIdentifierAlgorithm> m_SystemIdentifierAlgorithms = new List<SystemIdentifierAlgorithm>(
            Environment.OSVersion.Platform == PlatformID.Win32NT ?
                /*Algorithms to use on Windows:*/
                new SystemIdentifierAlgorithm[] { new NicIdentifierAlgorithm(), new ComputerNameIdentifierAlgorithm(), new HardDiskVolumeSerialIdentifierAlgorithm(HardDiskVolumeSerialFilterType.OperatingSystemRootVolume), new BiosUuidIdentifierAlgorithm(), new ProcessorIdentifierAlgorithm(new ProcessorIdentifierAlgorithmTypes[] { ProcessorIdentifierAlgorithmTypes.ProcessorName, ProcessorIdentifierAlgorithmTypes.ProcessorVendor, ProcessorIdentifierAlgorithmTypes.ProcessorVersion }) } :
                /*Algorithms to use on all other platforms (Note that you can add HardDiskVolumeSerialIdentifierAlgorithm for Linux, but it is not presently supported in OS X):*/
                new SystemIdentifierAlgorithm[] { new NicIdentifierAlgorithm(), new ComputerNameIdentifierAlgorithm() });
        private static WebServiceHelper m_WebServiceHelper = new WebServiceHelper();

        //When set to true, RefreshLicense will be called every time the application runs.
        private const bool m_RefreshLicenseAlwaysRequired = false;

        //TODO: Set this value per your licensing requirements!
        //  For example, to attempt to check and refresh the license with SOLO Server every 7 days, set this value to 7.
        private const Int32 m_RefreshLicenseAttemptFrequency = 7;

        //TODO: Set this value per your licensing requirements!
        // For example, to require the license to be checked and refreshed with SOLO Server every 14 days, set this value to 14.
        // Setting this value to zero to allows the application to make RefreshLicense attempts without requiring them.
        private Int32 m_RefreshLicenseRequireFrequency = 0;
        #endregion

        #region Member Variables
        private string m_LicenseFilePath = "";
        private string m_LicenseStatus = "";
        #endregion

        #region Constructors
        /// <summary>Default ClientLicense constructor</summary>
        public ReadOnlyLicense(string licenseFilePath, int productId)
            : base(m_EncryptionKey, true, true, productId, IOHelper.GetAssemblyFileVersion(Assembly.GetCallingAssembly()), m_SystemIdentifierAlgorithms)
        {
            //NOTE: the two boolean arguments where the base class's constructor is
            // called above are for using an encrypted License File and encrypting
            // web service calls, respectively.

            m_LicenseFilePath = licenseFilePath;

        }

        //network
        public ReadOnlyLicense(string licenseFilePath, object network, int productId)
            : base(m_EncryptionKey, true, true, productId, IOHelper.GetAssemblyFileVersion(Assembly.GetCallingAssembly()),
                    new List<SystemIdentifierAlgorithm>(new SystemIdentifierAlgorithm[] { new NetworkNameIdentifierAlgorithm(Path.GetDirectoryName(licenseFilePath.ToLower())) }))
        {
            //NOTE: the two Boolean arguments where the base class's constructor is
            // called above are for using an encrypted License File and encrypting
            // web service calls, respectively.

            m_LicenseFilePath = licenseFilePath;
        }
        #endregion

        #region Private Properties
        /// <summary>Whether the Refresh License Attempt is due or not</summary>
        private bool IsRefreshLicenseAttemptDue
        {
            get
            {
                //Calculate the date difference between signature date and the current date
                TimeSpan dateDiff = DateTime.UtcNow.Subtract(this.SignatureDate);

                //if (m_RefreshLicenseAlwaysRequired || (m_RefreshLicenseAttemptFrequency > 0 && (dateDiff.TotalDays > m_RefreshLicenseAttemptFrequency || dateDiff.TotalDays > m_RefreshLicenseRequireFrequency)))
                if (m_RefreshLicenseAlwaysRequired ||
                    (m_RefreshLicenseAttemptFrequency > 0 &&
                    (dateDiff.TotalDays > m_RefreshLicenseAttemptFrequency ||
                    (m_RefreshLicenseRequireFrequency > 0 && dateDiff.TotalDays > m_RefreshLicenseRequireFrequency))))
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>Whether the Refresh License is Required or not</summary>
        private bool IsRefreshLicenseRequired
        {
            get
            {
                //Calculate the date difference between signature date and the current date
                TimeSpan dateDiff = DateTime.UtcNow.Subtract(this.SignatureDate);

                if (m_RefreshLicenseAlwaysRequired || (m_RefreshLicenseRequireFrequency > 0 && dateDiff.TotalDays > m_RefreshLicenseRequireFrequency))
                {
                    return true;
                }

                return false;
            }
        }
        #endregion

        #region Public Properties
        /// <summary>License status</summary>
        public string LicenseStatus
        {
            get { return m_LicenseStatus; }
            set { m_LicenseStatus = value; }
        }

        /// <summary>Returns true if the current license is a trial license</summary>
        public bool IsTrial
        {
            get { return false; }
        }
        #endregion

        #region Public Methods
        /// <summary>Loads a license file and stores the path for later use</summary>
        /// <param name="path">string</param>
        /// <returns>bool</returns>
        public bool LoadFile()
        {
            return base.LoadFile(m_LicenseFilePath);
        }

        /// <summary>Returns true if the license is valid</summary>
        /// <returns>bool</returns>
        public bool Validate()
        {
            //Refresh the license if date difference is greater than m_RefreshLicenseAttemptFrequency (days).
            if (IsRefreshLicenseAttemptDue)
            {
                //If refresh license fails and date difference is greater than m_RefreshLicenseRequireFrequency (days) set license as invalid.
                if (!RefreshLicense() && IsRefreshLicenseRequired)
                {
                    return false;
                }
            }

            //Create a list of validations to perform.
            List<SystemValidation> validations = new List<SystemValidation>();

            //Add a validation to make sure there is no active system clock tampering taking place.
            validations.Add(new SystemClockValidation());

            //Validate the Product ID authorized in the license to make sure the license file was issued for this application.
            validations.Add(new LicenseProductValidation(this, ThisProductID));

            //Run all of the validations (in the order they were added), and make sure all of them succeed.
            foreach (SystemValidation validation in validations)
            {
                if (!validation.Validate())
                {
                    LastError = validation.LastError;
                    return false;
                }
            }

            // This is our custom check (will set LastError if needed)
            if (!this.ValidateIdentifiers())
            {
                return false;
            }

            //If we got this far, all validations were successful, so return true to indicate success and a valid license.
            return true;
        }

        /// <summary>Activates the license online</summary>
        /// <param name="licenseId">Int32</param>
        /// <param name="password">string</param>
        /// <returns>bool</returns>
        public bool ActivateOnline(Int32 licenseId, string password)
        {
            string licenseContent = "";

            //initialize the object used for calling the web service method
            XmlActivationService ws = m_WebServiceHelper.GetNewXmlActivationServiceObject();
            if (null == ws)
            {
                this.LastError = m_WebServiceHelper.LastError;
                return false;
            }

            //activate online using our endpoint configuration from app.config
            if (!this.ActivateInstallationLicenseFile(licenseId, password, ws, ref licenseContent))
                return false;

            return this.SaveLicenseFile(licenseContent);
        }

        /// <summary>Deactivates the installation online</summary>
        /// <returns>bool</returns>
        public bool DeactivateOnline()
        {
            //initialize the object used for calling the web service method
            XmlActivationService ws = m_WebServiceHelper.GetNewXmlActivationServiceObject();
            if (null == ws)
            {
                this.LastError = m_WebServiceHelper.LastError;
                return false;
            }

            bool successful = this.DeactivateInstallation(ws);

            if (successful)
            {
                File.Delete(m_LicenseFilePath);
            }

            return successful;
        }

        /// <summary>Returns true if it successfully refreshes the license file</summary>
        /// <returns>bool</returns>
        public bool RefreshLicense()
        {
            //initialize the object used for calling the web service method
            XmlLicenseFileService ws = m_WebServiceHelper.GetNewXmlLicenseFileServiceObject();
            if (null == ws)
            {
                this.LastError = m_WebServiceHelper.LastError;
                return false;
            }

            string licenseContent = "";
            if (!base.RefreshLicense(ws, ref licenseContent))
            {
                if (this.LastError.ExtendedErrorNumber == 5010 || this.LastError.ExtendedErrorNumber == 5015 || this.LastError.ExtendedErrorNumber == 5016 || this.LastError.ExtendedErrorNumber == 5017)
                {
                    File.Delete(m_LicenseFilePath);
                }
                return false;
            }

            //try to save the license file to the file system
            if (!SaveLicenseFile(licenseContent))
                return false;

            return true;
        }

        /// <summary>Saves a new license file to the file system</summary>
        /// <param name="lfContent">string</param>
        /// <returns>bool</returns>
        public bool SaveLicenseFile(string lfContent)
        {
            //try to save the license file to the file system
            try
            {
                RegistryWrap registry = new RegistryWrap(typeof(ReadOnlyLicense));
                var licenseDirectoryPath = registry.GetValue("Licence Path", string.Empty, Registry.LocalMachine);
                var licenceFilePath = Path.Combine(licenseDirectoryPath, AppHelper.SoftwareInfo.LicenceFileName);
                if (string.IsNullOrEmpty(licenseDirectoryPath))
                {
                    licenseDirectoryPath = Path.GetDirectoryName(m_LicenseFilePath) + "\\";
                    licenceFilePath = m_LicenseFilePath;
                }
                if (!Directory.Exists(licenseDirectoryPath))
                {
                    Directory.CreateDirectory(licenseDirectoryPath);
                }

                File.WriteAllText(licenceFilePath.StartsWith("file:///") ? licenceFilePath.Substring(8) : licenceFilePath, lfContent);
            }
            catch (Exception ex)
            {
                this.LastError = new LicenseError(LicenseError.ERROR_COULD_NOT_SAVE_LICENSE, ex);
                return false;
            }

            return true;
        }

        /// <summary>Returns true if the specified License feature is enabled, or false if it is disabled</summary>
        /// <param name="feature">License features</param>
        /// <returns>bool</returns>
        public bool GetFeatureEnabled(Int32 feature)
        {
            //int features = (this.UserDefinedNumber1 == 0) ? this.TriggerCodeFixedValue : this.UserDefinedNumber1;
            int features = this.UserDefinedNumber1;
            return ((features & feature) != 0);
        }

        /// <summary>Returns the specified attribute value of enum</summary>
        /// <param name="value">Enum</param>
        /// <returns>string</returns>
        public string GetDisplayName(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
        }

        private bool ValidateIdentifiers()
        {
            int formatSerials = 0;
            int formatSerialMatches = 0;
            int nics = 0;
            int nicMatches = 0;
            int computerNames = 0;
            int computerNameMatches = 0;
            int networkNames = 0;
            int networkNameMatches = 0;

            //calculate the number of authorized identifiers of each type, and calculate how many matches are found 
            foreach (SystemIdentifier authorizedIdentifier in this.AuthorizedIdentifiers)
            {
                SystemIdentifier matchingIdentifier = null;

                foreach (SystemIdentifier currentIdentifier in this.CurrentIdentifiers)
                {
                    //Use the SystemIdentifier class's == operator overload to compare the value hash and type 
                    if (currentIdentifier == authorizedIdentifier)
                    {
                        //we found a match 
                        matchingIdentifier = currentIdentifier;
                        break;
                    }
                }

                //update our counters 
                switch (authorizedIdentifier.Type)
                {
                    case "NicIdentifier":
                        nics++;
                        if (matchingIdentifier != null)
                            nicMatches++;
                        break;
                    case "HardDiskVolumeSerialIdentifier":
                        formatSerials++;
                        if (matchingIdentifier != null)
                            formatSerialMatches++;
                        break;
                    case "ComputerNameIdentifier":
                        computerNames++;
                        if (matchingIdentifier != null)
                            computerNameMatches++;
                        break;
                    case "NetworkNameIdentifier":
                        networkNames++;
                        if (matchingIdentifier != null)
                            networkNameMatches++;
                        break;
                }
            }

            //Make sure we have at least one match for each type of identifier we have authorized 
            if ((formatSerialMatches < 1 || computerNameMatches < 1) && networkNameMatches < 1)
            {
                LastError = new LicenseError(LicenseError.ERROR_LICENSE_SYSTEM_IDENTIFIERS_DONT_MATCH);
                return false;
            }

            return true;
        }
        #endregion
    }
}
