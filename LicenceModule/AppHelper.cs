﻿using System;
using System.Linq;
using System.Text;
using ArkkUtilities;
using Microsoft.Win32;
using com.softwarekey.Client.Licensing;
using com.softwarekey.Client.Licensing.Network;
using System.Threading;
using System.IO;
using System.Windows.Forms;

namespace LicenceModule
{
    public class AppHelper
    {
        #region Fields
        private static AboutWindow m_aboutWindow;
        private static Features m_Features = new Features();
        internal static string m_LicenseFilePath;
        private static readonly string LicenseDefaultPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\\ArkkSolutions\\";
        private static ReadOnlyLicense m_License = null;
        private static ISimpleTextEditorLicense m_CurrentLicense = null;
        private static bool m_IsLicenseValid = false;
        private static NetworkSemaphore m_semaphore;
        private static Thread t;
        internal static int m_LicenseProduct;
        readonly static DriveInfo[] AllDrives = DriveInfo.GetDrives();
        public static readonly SoftwareInfo SoftwareInfo;

        #endregion

        static AppHelper()
        {
            SoftwareInfo = new SoftwareInfo(new SoftwareTypeConfiguration());
        }

        public static bool IsCategoryValid(FileStream stream, Func<string, string> GetSchema)
        {
            var categoryString = TemplateModifier.GetTemplateVersion(stream);

            bool enabledSchema = false;

            foreach(var feature in m_Features.ListFeatures)
            {
                if (categoryString.Contains(GetSchema(feature.Key)) && CurrentEnabledFeature(feature.Key))
                {
                    enabledSchema = true;
                    break;
                }
            }

            return enabledSchema;
        }

        public static bool CurrentEnabledFeature(string feature)
        {
            return AllFeatures.CheckStatus(feature);
        }

        /// <summary>All features</summary>
        public static Features AllFeatures
        {
            get
            {
                return m_Features;
            }
        }
        public static bool HasLicenseSeat
        {
            get
            {
                var myProcesses = System.Diagnostics.Process.GetProcesses();
                var isTagImporterRun = myProcesses.Any(p => p.ProcessName == "ArkkTagImporter.vshost" || p.ProcessName == "ArkkTagImporter");
                if (IsNetworkPath(m_LicenseFilePath) && isTagImporterRun)
                    return true;
                if (IsNetworkPath(m_LicenseFilePath))
                    return (m_semaphore != null && m_IsLicenseValid);
                return m_IsLicenseValid;
            }
        }

        /// <summary>Licence</summary>
        public static ReadOnlyLicense License
        {
            set { m_License = value; }
            get
            {
                return m_License;
            }
        }

        /// <summary>Is licence trial or not</summary>
        public static bool IsTrial
        {
            get
            {
                return false; // return m_IsTrial;
            }
        }

        /// <summary>Is licence valid or not</summary>
        public static bool IsLicenseValid
        {
            get
            {
                return m_IsLicenseValid;
            }
        }

        public static string ApplicationName
        {
            get
            {
                if (Application.ExecutablePath.ToLower().Contains("word"))
                    return "Word\\";
                if (Application.ExecutablePath.ToLower().Contains("excel"))
                    return "Excel\\";
                return "";
            }
        }

        //Get root of LicenseFilePath and check if it is a networkPath, we check too for virtual machines
        public static bool IsNetworkPath(string path)
        {
            var root = Path.GetPathRoot(path);
            var isNetworkOrPath = AllDrives.Any(d => d.DriveType == DriveType.Network && d.Name == root);
            if (!isNetworkOrPath)
            {
                return IsVirtualMachine();
            }
            return isNetworkOrPath;
        }

        //Get if it is a Virtual Machine
        public static bool IsVirtualMachine()
        {
            using (var searcher = new System.Management.ManagementObjectSearcher("Select * from Win32_ComputerSystem"))
            {
                using (var items = searcher.Get())
                {
                    foreach (var item in items)
                    {
                        var manufacturer = item["Manufacturer"].ToString().ToLower();
                        if (manufacturer == "microsoft corporation"
                            || manufacturer.Contains("vmware")
                            || item["Model"].ToString() == "VirtualBox")
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public delegate void UpdateRibbonDelegate(Features features);

        public static bool InitInternalCache(string licenseRegistryLocation, string app = null, UpdateRibbonDelegate delegateRibbon = null, bool isCalledFromAboutWindow = false) // AddInServices.LICENSE_REGISTRY_LOCATION
        {
            GetLicenceFilePath(app);
            m_LicenseProduct = SoftwareInfo.ProductId;

            bool successful = InitializeFeaturesWithLicense(delegateRibbon);

            if (successful)
            {
                if (delegateRibbon != null)
                    delegateRibbon(AllFeatures);
            }
            else
            {
                if (!isCalledFromAboutWindow)
                {
                    ShowAboutWindow(delegateRibbon);
                }
            }

            if (AllFeatures.CheckStatus("Adapter"))
            {
                MyResolver myresolver = new MyResolver();
                myresolver.PreLoad("root", "2");
            }
            return successful;
        }

        public static void ShowAboutWindow(UpdateRibbonDelegate delegateRibbon)
        {
            t = new Thread(delegate ()
            {

                try
                {
                    m_aboutWindow = new AboutWindow(m_License, "Software\\Arkk Solutions", delegateRibbon);
                    m_aboutWindow.ShowDialog();
                }
                catch (Exception)
                {
                    if (System.Windows.Application.Current != null)
                    {
                        try
                        {
                            Thread.Sleep(5000);
                            System.Windows.Application.Current.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal,
                            (Action)(() =>
                            {
                                m_aboutWindow.ShowDialog();
                            }
                            ));
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message + "666");
                        }
                    }

                }

            });
            t.SetApartmentState(ApartmentState.STA);
            t.Start();
        }

        private static bool RefreshLicenseStatus()
        {
            m_IsLicenseValid = m_CurrentLicense.Validate();
            //If License is not valid
            if (!m_IsLicenseValid)
            {
                ReleaseRepositoryCache();
                m_License.LicenseStatus = "The licence is invalid or expired.";
                return false;
            }

            //If License is Trial
            if (m_CurrentLicense.IsTrial)
            {
                TimeSpan ts = m_CurrentLicense.EffectiveEndDate - DateTime.Now.Date;
                m_License.LicenseStatus = string.Format("Trial expires in {0} days.", Math.Round(ts.TotalDays, 0));
                return false;
            }

            //If License is not in a Network path
            if (!IsNetworkPath(m_LicenseFilePath))
                return GetRegisterInfo(false);

            //If License is in a Network path
            return IsTagImporterAndOfficeRunBySameUser() || CreateAndTryToOpenNetWorkSession();
        }

        private static bool IsTagImporterAndOfficeRunBySameUser()
        {
            var myProcesses = System.Diagnostics.Process.GetProcesses();
            var tagImportSessions = myProcesses.Where(p => p.ProcessName == "ArkkTagImporter").Select(pr => pr.SessionId);
            if (tagImportSessions.Any())
            {
                var isSameUser = new UserChecker().IsSameUser(tagImportSessions);
                if (isSameUser)
                {
                    var officeSessions = ApplicationName.Contains("Word") ? myProcesses.Where(p => p.ProcessName == "WINWORD").Select(pr => pr.SessionId) : myProcesses.Where(p => p.ProcessName == "EXCEL").Select(pr => pr.SessionId);
                    if (officeSessions.Count() > 1)
                    {
                        return new UserChecker().IsSameUser(officeSessions);
                    }
                }
            }
            return false;
        }

        private static bool CreateAndTryToOpenNetWorkSession()
        {
            if (m_semaphore == null)
            {
                var sema = SoftwareInfo.SemaphoreName;
                m_semaphore = new NetworkSemaphore(Path.GetDirectoryName(m_LicenseFilePath), sema,
                                                   m_License.QuantityOrdered, true, 15, true);


                if (!m_semaphore.Open() &&
                    m_semaphore.LastError.ErrorNumber == LicenseError.ERROR_NETWORK_LICENSE_FULL)
                // try to open a network session
                {
                    m_semaphore = null;
                    m_License.LicenseStatus = string.Format("Concurrent session limit reached. Licence permits {0} sessions.", m_License.QuantityOrdered);
                }
                else if (m_semaphore.LastError.ErrorNumber != LicenseError.ERROR_NONE)
                {
                    m_semaphore = null;
                    m_License.LicenseStatus = "Unable to establish a network session. ";
                }
                if (m_aboutWindow != null)
                    m_aboutWindow.UpdateStatus(m_License.LicenseStatus);
            }

            if (m_semaphore != null && m_semaphore.IsValid)
                return GetRegisterInfo(true);
            return false;
        }

        public static bool GetRegisterInfo(bool networkPath)
        {
            var registerInfo = new StringBuilder();

            //Check if first name is not empty and not unregistered
            if (m_License.Customer.FirstName != "" && m_License.Customer.FirstName != "UNREGISTERED")
            {
                registerInfo.Append("Registered To: ");

                //Append first name
                registerInfo.Append(m_License.Customer.FirstName);
            }

            //Check if last name is not empty and not unregistered
            if (m_License.Customer.LastName != "" && m_License.Customer.LastName != "UNREGISTERED")
            {
                if (registerInfo.ToString() == "")
                    registerInfo.Append("Registered To:");
                registerInfo.Append(" ");

                //Append last name
                registerInfo.Append(m_License.Customer.LastName);
            }

            //Check if company name is not empty and not unregistered
            if (m_License.Customer.CompanyName != "" && m_License.Customer.CompanyName != "UNREGISTERED")
            {
                if (registerInfo.ToString() == "")
                    registerInfo.Append("Registered To:");
                registerInfo.Append(" ");

                //Append company name
                registerInfo.Append("[" + m_License.Customer.CompanyName + "]");
            }
            //Add a new line
            if (registerInfo.ToString() != "")
                registerInfo.Append(Environment.NewLine);

            //Append licence ID
            registerInfo.Append("Licence ID: " + m_License.LicenseID);

            m_License.LicenseStatus = "Fully Licensed. " + Environment.NewLine + registerInfo;
            if (networkPath)
            {
                m_License.LicenseStatus += string.Format("{0}Seat {1} out of {2}", Environment.NewLine, m_semaphore.SeatsActive, m_License.QuantityOrdered);
                if (m_aboutWindow != null)
                    m_aboutWindow.UpdateStatus(string.Format("Seat {0} out of {1}", m_semaphore.SeatsActive, m_License.QuantityOrdered));

            }
            return true;
        }

        public static int GetProductOptionId()
        {
            return m_License.ProductOption.ProdOptionID;
        }

        public static bool InitializeFeaturesWithLicense(UpdateRibbonDelegate delegateRibbon = null)
        {
            m_License = IsNetworkPath(m_LicenseFilePath) ? new ReadOnlyLicense(m_LicenseFilePath, null, m_LicenseProduct)
                                                         : new ReadOnlyLicense(m_LicenseFilePath, m_LicenseProduct);
            m_CurrentLicense = m_License;

            bool successful = ReloadLicense(delegateRibbon);

            SplashScreen.InitializeFeatures(m_Features, m_License, successful);

            return successful;
        }

        public static bool ReloadLicense(UpdateRibbonDelegate delegateRibbon = null)
        {
            if (!m_License.LoadFile() && IsNetworkPath(m_LicenseFilePath))
            {
                m_License.LicenseStatus = "Invalid.  " + m_License.LastError.ErrorString;
                return false;
            }

            if (!m_License.LoadFile() && !IsNetworkPath(m_LicenseFilePath))
            {
                var licenceStatusDefaultPathError = m_License.LicenseStatus = "Invalid.  " + m_License.LastError.ErrorString;
                var licencePath = Path.GetDirectoryName(m_LicenseFilePath) + "\\";
                var isLicenceReload = false;
                if (licencePath == LicenseDefaultPath + ApplicationName)
                {
                    isLicenceReload = TryToReloadLicence(LicenseDefaultPath);
                }
                if (licencePath == LicenseDefaultPath)
                {
                    isLicenceReload = TryToReloadLicence(LicenseDefaultPath + ApplicationName, licenceStatusDefaultPathError);
                    if (isLicenceReload)
                        CopyLicenceFileToDefaultPathFolder();

                }
                if (!isLicenceReload)
                {
                    return false;
                }
            }

            m_CurrentLicense = m_License;

            return RefreshLicenseStatus();
        }

        private static bool TryToReloadLicence(string licenseDefaultPath, string licenceStatusDefaultPathError = null)
        {
            m_LicenseFilePath = Path.Combine(licenseDefaultPath, SoftwareInfo.LicenceFileName);
            m_License = new ReadOnlyLicense(m_LicenseFilePath, m_LicenseProduct);
            if (!m_License.LoadFile())
            {
                m_IsLicenseValid = false;
                var licenceStatusError = "Invalid.  " + m_License.LastError.ErrorString;
                m_License.LicenseStatus = string.IsNullOrEmpty(licenceStatusDefaultPathError) ? licenceStatusError : licenceStatusDefaultPathError;
                return false;
            }
            return true;
        }
        /// <summary>
        /// We are using the DefaultPath "C:\ProgramData\ArkkSolutions" for all Licences (no matter the SoftwareType). 
        /// If the Licence file is under "C:\ProgramData\ArkkSolutions\App" (where App is Excel/Word), we copy it into the DefaultPath.
        /// </summary>
        private static void CopyLicenceFileToDefaultPathFolder()
        {
            m_LicenseFilePath = Path.Combine(LicenseDefaultPath, SoftwareInfo.LicenceFileName);
            foreach (var file in Directory.GetFiles(LicenseDefaultPath + ApplicationName))
            {
                if (!File.Exists(m_LicenseFilePath) && Path.GetFileName(file) == SoftwareInfo.LicenceFileName)
                {
                    File.Copy(file, Path.Combine(LicenseDefaultPath, Path.GetFileName(file)), true);
                }
            }
            m_License = new ReadOnlyLicense(m_LicenseFilePath, m_LicenseProduct);
            m_License.LoadFile();
        }

        public static void GetLicenceFilePath(string app)
        {
            var registry = new RegistryWrap(typeof(AppHelper));

            var registryLicensePath = registry.GetValue("Licence Path", string.Empty, Registry.LocalMachine);
            if (!string.IsNullOrEmpty(registryLicensePath))
                m_LicenseFilePath = registryLicensePath;
            else
            {
                m_LicenseFilePath = LicenseDefaultPath;
                if (app != null && SoftwareInfo.SoftwareType == SoftwareType.Ixbrl)
                    m_LicenseFilePath = LicenseDefaultPath + app + "\\";
            }

            m_LicenseFilePath = Path.Combine(m_LicenseFilePath, SoftwareInfo.LicenceFileName);

        }

        static public void ReleaseRepositoryCache()
        {
            if (m_semaphore != null)
            {
                m_semaphore.Close(); // close our network session if it is open
                m_semaphore = null;
            }
        }
    }
}
